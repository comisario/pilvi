/*global angular  */

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var myApp = angular.module('myApp', ['ngRoute'])
var searchedRecent =[]

/* the config function takes an array. */
myApp.config( ['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/search', {
		  templateUrl: 'templates/search.html',
      controller: 'searchController'
		})
    //.when('/detail/:id', {
    .when('/detail/:book', {
      templateUrl: 'templates/detail.html',
      controller: 'detailController'
    })
    .when('/favourites', {
		  templateUrl: 'templates/favourites.html',
      controller: 'favouritesController'
		})
		//recent route
		.when('/recent', {
      templateUrl: 'templates/recent.html',
      controller: 'recentController'
    })
		.otherwise({
		  redirectTo: 'search'
		})
	}])


myApp.controller('searchController', function($scope, $http) {
  $scope.message = 'This is the search screen'
  $scope.search = function($event) {
    console.log('search()')
    if ($event.which == 13) { // enter key presses
      var search = $scope.searchTerm
       searchedRecent.push(search)
      console.log(search)
      var url = 'https://www.googleapis.com/books/v1/volumes?maxResults=40&fields=items(id,volumeInfo(title, authors))&q='+search
      $http.get(url).success(function(response) {
        console.log(response)
        $scope.books = response.items
        $scope.searchTerm = ''
      })
    }
  }
})
//recent route
myApp.controller('recentController', function($scope) {
  $scope.message = 'Recent searches:'
  $scope.searches = searchedRecent
  $scope.clearSearches = function() {
    console.log('clearing recent searches')
    searchedRecent = []
    $scope.searches = null
  }
})

myApp.controller('detailController', function($scope, $routeParams) {
  $scope.message = 'This is the detail screen'
  //$scope.id = $routeParams.id
   $scope.book = JSON.parse($routeParams.book)
  //$scope.addToFavourites = function(id) {
  //console.log('adding: '+id+' to favourites.')
  //localStorage.setItem(id, id)
  $scope.addToFavourites = function(book) {
  	console.log('adding: '+book.id+' to favourites.')
		localStorage.setItem(book.id, $routeParams.book)
    console.log(localStorage)
  }
})

myApp.controller('favouritesController', function($scope) {
  console.log('fav controller')
  $scope.message = 'This is the favourites screen'
  $scope.delete = function(id) {
    console.log('deleting id '+id)
    localStorage.removeItem(id)
    $route.reload()
  }
   $scope.clearAll = function() {
    console.log('clearing local storage')
    localStorage.clear()
    $route.reload()
  }
  var init = function() {
    console.log('getting books')
    var items = Array()
    for (var a in localStorage) {
      items.push(JSON.parse(localStorage[a]))
    }
    console.log(items)
    $scope.books = items
    console.log($scope.books)
  }
  init()
})
