
var app = angular.module('myApp', [])
app.controller('todoController', function($scope) {

  /* an array to store the list items */
  var items = Array()

  $scope.add = function($event) {
    console.log('add()')
    /* the $event object was passed from the view and contains useful information that can be used by the controller. */
    console.log('key code: '+$event.which)
    var keyCode = $event.which || $event.keyCode
    if (keyCode === 13) {
      console.log('enter key pressed')
      var newItem = $scope.newItem.trim()
      console.log(newItem)

      //Katsotaan loytyyko listasta
      if(items.indexOf(newItem)=== -1 && newItem){
      	items.push(newItem)
      	console.log('Item: ' +newItem+ ' added')
      }else{
      	console.log('Item '+newItem+ ' is already on the list')
      }

      console.log(items)
      $scope.newItem = ''
      $scope.items = items
    }
  }
  $scope.delete = function(item) {
  	 if(items.indexOf(item)>-1){
    console.log('delete()')
    var index = items.indexOf(item)
    items.splice(index, 1)
    console.log('item '+item+' deleted')
    console.log(items)
  }}
})