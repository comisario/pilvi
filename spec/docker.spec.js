describe('A spec using beforeAll', function() {
	var stone, book, cotton, bag, vuitton, schenker;

	beforeAll(function() {
		stone = new Stuff('stone', 3);
		book = new Stuff('book', 7);
		cotton = new Stuff('cotton', 0.001);
		bag = new Bag(10);
		vuitton = new Bag(3);
		schenker = new Cargo(15);
	});

	it('Laukun paino: pitäisi olla 3: ', function(){
		bag.add(stone);
		expect(bag.weight()).toBe(3);
	});

	it('Stuff lisätty jo, ei onnistu!', function() {
		bag.add(stone);
		expect(bag.weight()).toBe(3);
	});

	it('Laukun paino, pitäisi olla 10: ', function(){
		bag.add(book);
		expect(bag.weight()).toBe(10);
	});

	it('Liian painava, ei pysty!', function(){
		bag.add(cotton);
		expect(bag.weight()).toBe(10);
	});

	it('Ruuman paino, pitäisi olla 10: ', function(){
		schenker.add(bag);
		expect(schenker.weight()).toBe(10);
	});

	it('Vääränlainen esine, ei onnistu!', function(){
		schenker.add(cotton);
		expect(schenker.weight()).toBe(10);
	});

	it('Ruuman paino, pitäisi olla noin 10.001: ', function(){
		vuitton.add(cotton);
		schenker.add(vuitton);
		expect(schenker.weight()).toBe(10.001);
	});

	it('Ruuman paino, pitäisi olla 310: ', function(){
		cotton.weight = 300;
		expect(schenker.weight()).toBe(310);
	});


})