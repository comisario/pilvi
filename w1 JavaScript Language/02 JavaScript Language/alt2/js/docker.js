// koodi:
function Stuff(name, weight) {
	this.name = name;
	this.weight = weight;
}

function Bag(maxWeight) {
	this.max = maxWeight;
	this.stuffs = [];
	this.add = function(stuff) {
		if (this.stuffs.includes(stuff)) {
			console.log('Stuff lisätty jo, ei onnistu!');
		}
		else if (!(stuff instanceof Stuff)) {
			console.log('Vääränlainen esine, ei onnistu!');
		}
		else {
			if (this.weight() + stuff.weight > this.max) {
				console.log('Laukku liian painava, ei pysty!');
			} else {
				this.stuffs.push(stuff);
			}
		}
	};
	this.weight = function() {
		var w = 0;
		for (var i = 0; i < this.stuffs.length; i++) {
			w += this.stuffs[i].weight;
		}
		return w;
	};
}

function Cargo(maxWeight) {
	this.max = maxWeight;
	this.bags = [];
	this.add = function(bag) {
		if (this.bags.includes(bag)) {
			console.log('Bag lisätty jo, ei onnistu!');
		}
		else if (!(bag instanceof Bag)) {
			console.log('Vääränlainen laukku, ei onnistu!');
		}
		else {
			if (this.weight() + bag.weight() > this.max) {
				console.log('Cargo liian painava, ei pysty!');
			} else {
				this.bags.push(bag);
			}
		}
	};
	this.weight = function() {
		var w = 0;
		for (var i = 0; i < this.bags.length; i++) {
			w += this.bags[i].weight();
		}
		return w;
	};
}

function add(object, weight, maxweight){
	if(weight+ object.weight <= maxweight){
		return weight + object.weight;
	}else{
		return weight;
	}
}

var stone = new Stuff("stone", 3);
var book = new Stuff("book", 7);
var cotton = new Stuff("cotton", 0.001);

var bag = new Bag(10);
var vuitton = new Bag(3);

var schenker = new Cargo(15);


bag.add(stone);
console.log("laukun paino, pitäisi olla 3: " + bag.weight());
bag.add(stone); // virhe: "Stuff lisätty jo, ei onnistu!"

bag.add(book);
console.log("laukun paino, pitäisi olla 10: " + bag.weight());

bag.add(cotton); // virhe: "Liian painava, ei pysty!"

console.log("laukun paino, pitäisi olla 10: " + bag.weight());


schenker.add(bag);
schenker.add(cotton); // virhe: Vääränlainen esine, ei onnistu!

console.log("Ruuman paino, pitäisi olla 10: " + schenker.weight());

vuitton.add(cotton);
schenker.add(vuitton);
console.log("Ruuman paino, pitäisi olla noin 10.001: " + schenker.weight());

cotton.weight = 300;
console.log("Ruuman paino, pitäisi olla 310: " + schenker.weight()); // hups!
