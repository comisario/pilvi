var fs = require('fs')
var rewire = require('rewire')
var taxi = rewire('../modules/taxi')

describe('Long Length Route', function () {

	/* the routedata comes from an external API that is not guaranteed to return consistent data. We substitute a different function for testing that returns a fixed object. */
	taxi.__set__('getRouteData', function(start, end) {
		console.log('MOCK 1')
		const data = fs.readFileSync('spec/routedata/cov_car_cas.json', "utf8")
		return JSON.parse(data)
	})

	it('should set Gulson Road, Coventry as the current location', function(done) {
		taxi.setHome('Gulson Road, Coventry', function(data) {
			expect(data.lat).toEqual(52.405849)
			expect(data.lng).toEqual(-1.496193)
			done()
		})
	})

	it('should calculate the fare to Carlisle Castle, Carlisle', function(done) {
		taxi.getFare('Carlisle Castle, Carlisle', function(data) {
			expect(data.distance).toEqual(348536)
			expect(data.duration).toEqual(13130)
			expect(data.cost).toEqual(651.55)
			done()
		})
	})

})
